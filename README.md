# UnLId 2023

Dagstuhl seminar "Universals of Linguistic Idiosyncrasy in Multilingual Computational Linguistics".

For more details read the [wiki](https://gitlab.com/unlid-dagstuhl-seminar/unlid-2023/-/wikis/home).

