\section{Goals}
\begin{itemize}
  \item To extend the UD representation in a way that can accommodate complex morphosyntactic phenomena
  \item To provide a proof of concept annotation for languages with no 1:1 mapping between segments and morphosyntactic nodes
  \item To improve parallel representation of argument structure between languages with radically different realization mechanisms (polysynthetic vs isolating)
  \item To discuss the role / contribution of derivational morphology
  \item To discuss how morphological marking complements compound and MWE
  \item To help field linguists who want to represent segmentation down to morphs
  \item To ultimately propose guidelines for cross-linguistically consistent annotation of polysynthetic (and in particular polypersonal agreement, noun-incorporating) languages
\end{itemize}

\section{Mechanisms that have been proposed}
\begin{itemize}
  \item “Empty nodes” (= abstract nodes) for indicating pronominal feature bundles
  \item “Empty nodes” carrying the lemma of an incorporated noun (not necessarily a linear segment
  \item A layered representation of phrase-level features for each (lexical) node which is distinct from the word-level features that the lexical item contributes. (“Layered” as in layered features, which exist as language-specific in UD, and have also been introduced in UniMorph 4.0.)
  \item Linear segmentation (of clitics etc) is optional, not mandatory
\end{itemize}

\section{Implication}

To allow for the free use of these mechanisms to express non-explicit morphological phenomena, and still stay faithful to the UD guidelines, it has been proposed that the kind of representation we develop is a part of the enhanced, rather than basic, UD trees.

Empty (abstract) nodes are part of the enhanced UD graph (but not of the basic UD tree). (Note: They are called \emph{empty} nodes in the current UD documentation but it is a misnomer because they often are not really empty, they may have a word form, UPOS tag etc. So we propose that UD should switch to a different term, such as \emph{abstract} nodes.) Enhanced UD already contains abstract nodes that represent elided predicates in gapping constructions. If we now add abstract nodes for a different purpose, namely to represent segments of surface words, we need a way of distinguishing different types of abstract nodes. We also need to identify the surface word to which the segment (abstract node) belongs. Therefore, the segment-abstract nodes could be identified by PartOf=ID in the MISC column, where ID is the ID of a regular node in the basic UD tree.

Enhanced UD seems to be a suitable area where segments and their relations could be represented. It is still part of the UD specification (as opposed to add-ons built on top UD, using the CoNLL-U Plus format), meaning that such data could be part of official UD releases. At the same time, the Enhanced UD guidelines are less developed and frozen than the Basic UD guidelines, so it should be easier to add new guidelines here. Enhanced annotation is considered optional in UD corpora and can be easily separated from the basic annotation, hence the additional complexity can be completely transparent for users who are not interested in it. These are the main reasons why we propose to use the enhanced representation for subword relations, as opposed to the multi-word token mechanism (MWT), which would lead to the new annotation being visible also in the basic representation. (Moreover, the assumption about MWT is that an orthographic word is split into multiple morphosyntactic words. If we also use it to further split morphosyntactic words into morphs, we will have to solve the problem of distinguishing the different levels of granularity and different types of units.)

We have not come to a conclusion about the labels of the relations between subword units. We have identified two levels of granularity that might be of interest:

\begin{itemize}
  \item Decomposition to lexical units: compounds and incorporation
  \item Complete segmentation to morphs
\end{itemize}

\section{Technical details of the proposal}

\begin{itemize}
  \item Phrase-level features are put to the MISC column of the node whose subtree contains all nodes that belong to the phrase. Not all nodes in the subtree belong necessarily to the phrase, so the phrase has to be specified using node ids. It can be discontinuous. A possible representation is like this: \texttt{Phrase=1,3-5,7}
  \item The features have to be distinguished from other things that may be present in the same MISC cell. For UD-style features, prefixing the feature name with “Phrase” might work: \texttt{PhraseAspect=Prog|PhraseTense=Pres}. Datasets that use UniMorph-style features instead might need just one string: \texttt{PhraseUniMorph=V;PROG;PRS}.
  \item UD documentation should retire the term “empty node” and switch to “abstract node”, as these nodes often are not empty in the sense of not having any lexical or morphological value.
  \item If the dataset contains segmentation of syntactic words to smaller units that are not syntactic words, the abstract nodes should be used. Consequently, the segmentation is only visible in the enhanced representation while the basic tree stays reasonably simple.
  \item As abstract nodes are already used for other purposes than morphological segmentation, the nodes resulting from segmentation should be distinguished from other abstract nodes. Specifically, an abstract node (of the old kind) is not considered to correspond to any part of any surface token. The rule for the abstract nodes resulting from segmentation would be that they appear between the node corresponding to the surface token they are part of, and the next node (abstract or regular). Each abstract node resulting from segmentation would have in MISC a reference to its corresponding surface token or syntactic word: \texttt{PartOf=2}.
  \item While an abstract segment-node knows to which surface token it belongs, it does not have to declare precisely which substring of the surface token it represents. Consequently, the order of the abstract nodes is not prescribed, although annotators are encouraged to follow the ordering of the corresponding morphs where it is observable.
  \item The abstract segment-nodes have to be connected in the enhanced graph, if for nothing else, then to maintain compatibility with Enhanced UD. It is yet to be seen whether and to what extent it is useful to define “syntactic” relations between the segments. As a minimum, the main lexical root has to be declared as the head and the other segments can be attached directly as its dependents. The relation labels (deprels) have to be taken from the UD repository. In some cases \texttt{compound} could be used. Cases with no better option could use a subtype of \texttt{dep}, such as \texttt{dep:infl}.
    \begin{itemize}
      \item Languages with incorporation will attach the incorporated segments as core arguments of the verb: \texttt{obj}.
      \item Another possible usage of the relations between segments is to show the order of derivation. CCG-like categories might then be added to MISC to signal that this morph combines with a \texttt{VERB} and once combined, the result is an \texttt{ADJ}.
      \item Also note that some languages seem to have examples where another word would modify just one segment of the current word (Turkish \textit{mavi arabadakiler} “those in the blue car”; lit. “blue car-in-those”). Here the enhanced graph would have an \texttt{amod} relation between \textit{araba} and \textit{mavi}, while in the basic tree it would go directly between \textit{arabadakiler} and \textit{mavi}.
    \end{itemize}
  \item Two possible levels of segmentation are envisioned (both are optional, so people do not have to segment if they do not want to): 1. just split compounds (including incorporated nouns); 2. segment all the way down to morphs. Splitting compounds is useful for cross-linguistic parallelism. Complete segmentation is useful for field linguists who want to represent it, including features and glosses. And of course, there is a third level of segmentation, which already exists in UD: multi-word orthographic tokens are split to syntactic words; this one is done in basic UD and does not require abstract nodes.
  \item The FORM of an abstract segment can be empty (underscore), but it can be non-empty where it makes sense. The LEMMA should have a canonical form of that segment (e.g. English prefixes \textit{in-} and \textit{im-} would share the canonical form \textit{in}).
  \item It is possible to say that some forms in a paradigm table are segmentable while others are not. For instance, one could say that the English verb \textit{closed} is segmented to \textit{close + d}, where the suffix is the bearer of the feature \texttt{Tense=Past}, but if the verb is irregular like \textit{made}, it can stay unsegmented and bear the feature \texttt{Tense=Past} as a whole.
  \item If a user/application only wants to work with basic trees, the segmentation will be transparent for them. However, it is also possible that they want to work with the enhanced graph for other reasons but they still do not want to see the segmentation (or they want to see the compound level but not the complete decomposition to morphs). For that purpose we need an algorithm that will only extract the enhanced graph over full syntactic words. This has to be worked out. (Also, both the head of the subtree of segments and the original unsegmented word need to be attached somewhere in the enhanced graph.)
  \item In general it would be useful if one can say what is the backbone tree in the enhanced graph, and which edges are extra (creating reentrancies and cycles). This is currently not possible; one would have to modify the labeling schema for enhanced relations. Note that the backbone enhanced tree is not necessarily identical to the basic UD tree, as it can contain abstract nodes. The current proposal does not (yet) say how this should be done.
  \item Note that an abstract node may be also needed to represent a participant of an event (subject, object, oblique) which is not overtly represented as a word. This may correspond to an abstract segment-node under the verb, if the participant is referenced by the verbal morphology, but it is also possible that there is no trace of it in morphology and we still need to represent it e.g. to annotate coreference. (Conversely, in some languages a participant may be overtly referenced multiple times in the same clause: clitic doubling, full noun phrase, and verbal morphology.)
\end{itemize}

\section{References to related UD issues}
\begin{itemize}
  \item \url{https://github.com/UniversalDependencies/docs/issues/701}
  \item \url{https://github.com/UniversalDependencies/docs/issues/703}
  \item \url{https://github.com/UniversalDependencies/docs/issues/704}
\end{itemize}

\section{Future work}
\begin{itemize}
  \item Do a pilot segmentation of compounds in Parallel UD treebanks (PUD, 1000 news and wikipedia sentences per language). Try a subset of PUD languages, especially those with lots of compounding, such as German and Swedish. Examine parallelism in word (morph) alignment.
  \item Convert UD morphological features in UD treebanks to UniMorph (update the conversion procedure, originally tested with UniMorph 2.0, to the latest set of UD features and the latest version of UniMorph (4.0). For each word form, compare the UD annotation with the corresponding entry in UniMorph word lists, if available. Possibly improve UD and/or UniMorph data based on the comparison. Prepare a new version of UniMorph where a new column will say for each word form its frequency in UD treebanks.
    \begin{itemize}
      \item Expand the UD-UniMorph comparison to phrase-level morphology, such as periphrastic tense, aspect or voice.
    \end{itemize}
\end{itemize}

\begin{thebibliography}{0}
  \bibitem{baldwin} Timothy Baldwin, William Croft, Joakim Nivre, and Agata Savary. 2021. Universals of Linguistic Idiosyncrasy in Multilingual Computational Linguistics (Dagstuhl Seminar 21351). Dagstuhl Reports, 11(7):89–138.
  \bibitem{haspelmath} Martin Haspelmath. 2022 Draft. Defining the Word.
  \bibitem{kahane} Sylvain Kahane, Martine Vanhove, Rayan Ziane, and Bruno Guillaume. 2021. A morph-based and a word-based treebank for Beja. In Proceedings of the 20th International Workshop on Treebanks and Linguistic Theories (TLT, SyntaxFest 2021), pages 48–60, Sofia, Bulgaria. Association for Computational Linguistics.
  \bibitem{marsan} Büşra Marşan, Salih Furkan Akkurt, Muhammet Şen, Merve Gürbüz, Onur Güngör, Şaziye Betül Özateş, Suzan Üsküdarlı, Arzucan Özgür, Tunga Güngör, and Balkız Öztürk. 2022. Enhancements to the BOUN treebank reflecting the agglutinative nature of Turkish. arXiv preprint arXiv:2207.11782.
  \bibitem{tyers} Francis Tyers and Karina Mishchenkova. 2020. Dependency annotation of noun incorporation in polysynthetic languages. In Proceedings of the Fourth Workshop on Universal Dependencies (UDW 2020), pages 195–204, Barcelona, Spain (Online). Association for Computational Linguistics.
\end{thebibliography}
